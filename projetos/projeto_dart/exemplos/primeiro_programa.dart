import 'dart:io';
import 'package:intl/intl.dart';

main() {
	var fmt = NumberFormat('#,##0.00', 'pt_BR');
	
/*	stdout.write('Informe um nº ');
	String temp = stdin.readLineSync();
	int i = int.parse(temp);
	print('Este é um nº informado: $i');
*/	
	stdout.write('Informe o valor nº ');
	var d = fmt.parse(stdin.readLineSync());
	print('Este é um valor informado: ${fmt.format(d)}');
	
/*	stdout.write('Informe o 3º nº ');
	print('Este é um nº informado: ${num.parse(stdin.readLineSync())}');
*/}