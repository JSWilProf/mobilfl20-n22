import '../../lib/biblioteca.dart';

main() {
	var meusColegas = <Pessoa>[
		Colega()
				..nome = leTexto('Informe o nome')
				..email = leTexto('Informe o email'),
		Colega.comNomeEmail(leTexto('Informe o nome'),
												leTexto('Informe o email')),
		ColegaDeTrabalho(nome: leTexto('Informe o nome'))
	];
	
	
	meusColegas.forEach((colega) => print(colega));
}

class Pessoa {
	@override
  String toString() {
    return 'Sem Nome';
  }
}

class ColegaDeTrabalho extends Pessoa {
	final String nome;
	final String email;
	
	ColegaDeTrabalho({this.nome, this.email});
}

class Colega extends Pessoa {
	String nome;
	String email;
	
	Colega();
	
	Colega.comNomeEmail(String nome, String email) {
		this.nome = nome;
		this.email = email;
	}

	@override
  String toString() {
    return 'Nome: $nome E-Mail: $email';
  }
}