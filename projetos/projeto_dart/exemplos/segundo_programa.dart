import '../lib/biblioteca.dart';

main() {
  var valor = leDecimal('Informe o valor');

  if (valor > 500) {
	    print('O valor atualizado é R\$ ${fmt.format(valor * 1.05)}');
  } else {
	    print('O valor R\$ (${fmt.format(valor)}) não foi atualizado');
  }
}