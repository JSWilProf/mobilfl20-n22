import '../lib/biblioteca.dart';
import 'package:ordered_set/ordered_set.dart';
import 'package:ordered_set/comparing.dart';

main() {
	var textos = OrderedSet<String>(
		Comparing.reverse(
			Comparing.on( (texto) => texto )
			)
		);
	
	for(var i = 0;i < 3;) {
		var adicionou = textos.add(leTexto('Informe o ${i+1}º Texto'));
		print(adicionou);
		if(adicionou) i++;
	}
		
/*	for(var i in textos) {
		print(i);
	}
*/	//TODO: Como ordenar um Set em Dart
	// Ordered_set
	textos.forEach((t) => print(t));
}