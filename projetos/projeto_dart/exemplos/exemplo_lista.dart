import '../lib/biblioteca.dart';

main() {
	var numeros = <int>[];
	
	for(var i = 0;i < 3;i++) {
		numeros.add(leInteiro('Informe o ${i+1}º nº'));
	}
	
	for(var i = 0;i < numeros.length;i++) {
		print(numeros[i]);
	}
	
	for(var i in numeros) {
		print(i);
	}
	
	numeros.sort();
	numeros.forEach((i) => print(i));
}