import '../lib/biblioteca.dart';

main() {
	var total = 0.0;

	for(var i = 1;i <= 3;i++) {	
		total += leInteiro('Informe a Qtd $i') 
		       * leDecimal('Informe o Valor $i');
	}

	print('O Total é R\$ ${fmt.format(total)}');
}