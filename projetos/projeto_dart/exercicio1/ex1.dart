import '../lib/biblioteca.dart';

main() {
	var qtd1 = leInteiro('Informe a Qtd 1');
	var val1 = leDecimal('Informe o Valor 1');
	
	var qtd2 = leInteiro('Informe a Qtd 2');
	var val2 = leDecimal('Informe o Valor 2');
	
	var qtd3 = leInteiro('Informe a Qtd 3');
	var val3 = leDecimal('Informe o Valor 3');
	
	var total = qtd1 * val1 + qtd2 * val2 + qtd3 * val3;
	
	print('O Total é R\$ ${fmt.format(total)}');
}