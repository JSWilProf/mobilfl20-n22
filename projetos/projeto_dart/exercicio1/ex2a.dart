import '../lib/biblioteca.dart';

main() {
	var horas = leInteiro('Informe a Qtd de Horas Trabalhadas');
	var salHora = leDecimal('Informe o Salário Hora');
	var dep = leInteiro('Informe o nº de Dependentes');
	
	var salBruto = horas * salHora + 50 * dep;
	
	var inss = salBruto <= 1000 
					 ? salBruto * 0.085 
				   : salBruto * 0.09;

	var ir = salBruto <= 500 
	       ? 0.0 
         : salBruto <= 1000
           ? 0.05
           : salBruto * 0.07;
	
	var salLib = salBruto - inss - ir;
	
	print(
'''
	Salário Bruto: R\$ ${fmt.format(salBruto)}
	Desconto INSS: R\$ ${fmt.format(inss)}
	Desconto IR: R\$ ${fmt.format(ir)}
	Salário Líquido: R\$ ${fmt.format(salLib)}
''');
}



















