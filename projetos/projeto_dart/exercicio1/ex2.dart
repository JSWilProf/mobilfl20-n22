import '../lib/biblioteca.dart';

main() {
	var horas = leInteiro('Informe a Qtd de Horas Trabalhadas');
	var salHora = leDecimal('Informe o Salário Hora');
	var dep = leInteiro('Informe o nº de Dependentes');
	
	var salBruto = horas * salHora + 50 * dep;
	
	var inss;
	if(salBruto <= 1000) {
		inss = salBruto * 0.085;
	} else {
		inss = salBruto * 0.09;
	}

	var ir;
	if(salBruto <= 500) {
		ir = 0.0;
	} else if(salBruto <= 1000) {
		ir = 0.05;
	} else {
		ir = salBruto * 0.07;
	}
	
	var salLib = salBruto - inss - ir;
	
	print(
'''
	Salário Bruto: R\$ ${fmt.format(salBruto)}
	Desconto INSS: R\$ ${fmt.format(inss)}
	Desconto IR: R\$ ${fmt.format(ir)}
	Salário Líquido: R\$ ${fmt.format(salLib)}
''');
}



















