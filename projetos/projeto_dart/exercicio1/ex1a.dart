import '../lib/biblioteca.dart';

main() {
	var total = 0.0;
	
	total += leInteiro('Informe a Qtd 1') 
	       * leDecimal('Informe o Valor 1');
	
	total += leInteiro('Informe a Qtd 2') 
				 * leDecimal('Informe o Valor 2');
			
	total += leInteiro('Informe a Qtd 3') 
				 * leDecimal('Informe o Valor 3');

	print('O Total é R\$ ${fmt.format(total)}');
}